<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace common\widgets;

use common\models\WidgetCarousel;
use common\models\WidgetCarouselItem;
use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

/**
 * Class DbCarousel
 * @package common\widgets
 */
class DbOwlCarousel extends Widget
{
    /**
     * @var
     */
    public $key;

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (!$this->key) {
            throw new InvalidConfigException;
        }
        $cacheKey = [
            WidgetCarousel::className(),
            $this->key
        ];

        $content = Yii::$app->cache->get($cacheKey);
        if ($content === false) {
            $query = WidgetCarouselItem::find()
                ->joinWith('carousel')
                ->where([
                    '{{%widget_carousel_item}}.status' => 1,
                    '{{%widget_carousel}}.status' => WidgetCarousel::STATUS_ACTIVE,
                    '{{%widget_carousel}}.key' => $this->key,
                ])
                ->orderBy(['order' => SORT_ASC]);
            foreach ($query->all() as $k => $item) {
                /** @var $item \common\models\WidgetCarouselItem */
                $content .= '<div class="slideWrapperInn">';
                    if ($item->path) {
                        $content .= Html::img(
                                Yii::$app->glide->createSignedUrl([
                                        'glide/index',
                                        'path' => $item->path,
                                ], true),
                                ['alt' => Html::encode($item->caption), 'class'=>'desktopVisual']
                        );
                        $content .= Html::img(
                                Yii::$app->glide->createSignedUrl([
                                        'glide/index',
                                        'path' => $item->path_mobile,
                                ], true),
                                ['alt' => Html::encode($item->caption), 'class'=>'mobileVisual']
                        );
                    }

                    // $content .= '<div class="slideContentWrap"><div class="wrap1024"><div class="slideInfo">';
                    //     if ($item->caption) {
                    //         $content .= $item->caption;
                    //     }
                    // $content .= '</div></div></div>';
                $content .= '</div>';
            }
            Yii::$app->cache->set($cacheKey, $content, 60*60*24*365);
        }

        if (!empty($content)) {
            $this->getView()->registerJsFile('@web/js/carousel/owl.carousel.min.js', [ 'position' => \yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()] ]);
            $this->getView()->registerJsFile('@web/js/carousel.js', [ 'position' => \yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()] ]);
            $this->getView()->registerCssFile('@web/js/carousel/owl.carousel.css');
            return Html::tag('div', $content, ['class'=>'big-slider owl-carousel owl-'.$this->key, 'id'=>'owl-carousel']);
        }
    }
}
