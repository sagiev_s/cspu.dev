<?php
namespace common\widgets;
use common\models\Category;
use yii\base\Widget;
class CategoriesWidget extends Widget
{
    /**
     * @var Category
     */
    public $category;
    public function run()
    {
        $categories = Category::find()->orderBy('id')->all();
        $items = $this->getItemsRecursive($categories, null, $this->category);
        return $this->render('categories', [
            'items' => $items,
        ]);
    }
    /**
     * @var Category[] $categories
     * @param integer $parentId
     * @var Category $current
     * @return array
     */
    private function getItemsRecursive(&$categories, $parentId, $current)
    {
        $items = [];
        foreach ($categories as $category) {
            if ($category->parent_id == $parentId) {
                $items[] = [
                    'label' => $category->name,
                    'url' => ['/site/category', 'id' => $category->id],
                    'active' => $current && $category->id == $current->id ? true : null,
                    'items' => $this->getItemsRecursive($categories, $category->id, $current),
                ];
            }
        }
        return $items;
    }
} 