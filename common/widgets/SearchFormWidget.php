<?php
	namespace common\widgets;
	use yii\widgets\ActiveForm;
	use yii\db\Query;
	use yii\jui\AutoComplete;
	use common\models\Search;
	use yii\base\Widget;
	use Yii;
	use yii\helpers\Html;

class SearchFormWidget extends Widget
{

    public $inputclass = 'treeSearchbar';
    public $btnclass = 'treeSearchbtn';
    public $inputWrap = 'searchinputs';
    public function run()
    {
    $form = ActiveForm::begin(
        [
        'action' => ['/site/search'],
        'method' => 'post',
        ]);
    $listdata = (new Query())
        ->select(['code as value', 'concat(code, " ", name) as label'])
        ->from('category')
        ->all();
    $listdata1 = (new Query())
        ->select(['code as value', 'concat(code, " ", name) as label'])
        ->from('product')
        ->all();
    $list = array_merge($listdata, $listdata1);
    $smodel = new Search();
?>
	<div class="<?= $this->inputWrap ?>">
		<?= $form->field($smodel, 'search')->widget(AutoComplete::classname(), [
        'options' => [
            'class' => "$this->inputclass",
            'placeholder' => Yii::t('frontend','Enter first 4 digits of code'),
        ],
        'clientOptions' => [
            'source' => $list,
            'minLength'=>'4',
        ],
    ])->label(false);    
    ?><?=Html::submitInput(Yii::t('frontend', 'search'), ['class' => "$this->btnclass"]) ?>

	</div>
	<?php ActiveForm::end(); 
    }
}
?>