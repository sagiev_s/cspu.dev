<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property string $title
 * @property string $format
 * @property string $language
 * @property string $file_base_url
 * @property string $file_path
 * @property integer $sorter
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Document extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url'
            ],
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sorter'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'format', 'language'], 'required'],
            [['sorter', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title', 'file_base_url', 'file_path'], 'string', 'max' => 255],
            [['format'], 'string', 'max' => 20],
            [['language'], 'string', 'max' => 10],
            [['file'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'format' => Yii::t('app', 'Format'),
            'language' => Yii::t('app', 'Language'),
            'file_base_url' => Yii::t('app', 'File Base Url'),
            'file_path' => Yii::t('app', 'File Path'),
            'sorter' => Yii::t('app', 'Sorter'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'file' => Yii::t('app', 'Attachment'),
        ];
    }
}
