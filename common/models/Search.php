<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Product;


class Search extends \yii\base\Model
{
    public $search;
    
    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }
    public function getSearch()
    {
        return $this->search;
    }
}