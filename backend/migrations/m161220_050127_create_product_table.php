<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m161220_050127_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->string(),
            'active' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'parent_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'created_by' => $this->string(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->string()
        ]);

        $this->createIndex('idx-product-category_id', '{{%product}}', 'category_id');
        $this->createIndex('idx-product-parent_id', '{{%product}}', 'parent_id');
        $this->addForeignKey('fk-product-parent', '{{%product}}', 'parent_id', '{{%product}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('fk-product-category', '{{%product}}', 'category_id', '{{%category}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%product}}');
    }
}
