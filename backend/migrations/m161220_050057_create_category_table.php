<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m161220_050057_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'parent_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'created_by' => $this->string(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->string()
        ]);

        $this->createIndex('idx-category-parent_id', '{{%category}}', 'parent_id');
        $this->addForeignKey('fk-category-parent', '{{%category}}', 'parent_id', '{{%category}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
