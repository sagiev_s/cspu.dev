<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\sortablegrid\SortableGridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Document',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id', 
                'options' => ['style' => 'width: 50px; text-align: center;'],
            ],
            'title',
            'format',
            'language',
            'created_at:datetime',
            'sorter',
            // 'file_path',
            // 'sorter',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
