<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Document */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Document',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
