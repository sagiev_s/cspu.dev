<?php
namespace frontend\controllers;

use Yii;
use common\models\Questions;
use yii\web\Controller;
use common\models\Product;
use common\models\Search;
use common\models\Category;
use common\models\Page;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\db\Query;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }
    public function actionIndex()
    {
        $documents = \common\models\Document::find()->where(['language'=>Yii::$app->language])->orderBy('sorter')->all();
        $newsCat = 1;
        if(LANG=='en') {
            $newsCat = 3;
        } elseif(LANG=='kz') {
            $newsCat = 2;
        } else {
            $newsCat = 1;
        }
        $news = \common\models\Article::find()->where(['status'=>1, 'category_id'=>$newsCat])->orderBy('published_at DESC')->limit(3)->all();
        return $this->render('index', [
            'documents' => $documents,
            'news' => $news,
            'newsCategory' => \common\models\ArticleCategory::findOne($newsCat),
            ]);   
    }
    public function actionTree()
    {
        $categories = Category::find()->all();
        return $this->render('tree', [
                'categories' => $categories,
            ]);   
    }
    public function actionCodeInfo($id) 
    {
        $products = Product::findOne($id);
        echo Json::encode($products);
    }
    public function actionCategoryInfo($id) 
    {
        $category = Category::findOne($id);
        echo Json::encode($category);
    }
    public function actionTrees()
    {
        $dataProvider = new ActiveDataProvider ([
            'query' => Product::find()->with(['category'])->active()
        ]);
        return $this->render('trees', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCategory($id)
    {
        $category = $this->findCategoryModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->active()->forCategory($category->id),
            ]);
        return $this->render('category', [
            'category' => $category,
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionContact()
    {
        $content = Page::find()->where(['slug'=>'contacts', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('contact', [
            'content' => $content,
        ]);
    }
    public function actionSearch()
    {
        $smodel = new Search;

        if($smodel->load(Yii::$app->request->post()))
        {
            $search = $smodel->search;
            if(preg_match('/([0-9]){4}/', $search)) {
                if(strlen($search)<7) {
                    $query = Category::find();
                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                ]);
                //$query->andFilterWhere(['code' => $search]);
                $query->andFilterWhere(['or', ['like', 'code', $search]]);
                }
                else {
                    $query = Product::find()->with('category');
                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                ]);
                $query->andFilterWhere(['or', ['like', 'code', $search]]);
                }
            }
            else {
                $query = Product::find()->joinWith('category');
                $dataProvider = new ActiveDataProvider([
                        'query' => $query,
                ]);
                $query->andFilterWhere(['or', ['like', 'product.name', $search], ['like', 'category.name', $search]]);
            }   
        }

        return $this->render('search', [
            'search' => $search,
            'smodel' => $smodel,
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionServices()
    {
        $content = Page::find()->where(['slug'=>'service', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('services', [
                'content' => $content
            ]);
    }
    public function actionAbout()
    {
        $content = Page::find()->where(['slug'=>'about', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('about',[
            'content' => $content,
        ]);
    }
    public function actionPage($slug)
    {
        $content = Page::find()->where(['slug'=>$slug, 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('staticpage',[
            'content' => $content,
        ]);
    }
    public function actionCertificates()
    {
        $content = Page::find()->where(['slug'=>'certificates', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('certificates', [
            'content' => $content,
        ]);
    }
    public function actionPartners()
    {
        $content = Page::find()->where(['slug'=>'partners', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('partners', [
            'content' => $content,
        ]);
    }
    public function actionInformaciaObOpsPu()
    {
        $content = Page::find()->where(['slug'=>'informacia-ob-ops-pu', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('informacia-ob-ops-pu', [
            'content' => $content,
        ]);
    }
    public function actionInformaciaObOpsSm()
    {
        $content = Page::find()->where(['slug'=>'informacia-ob-ops-sm', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('informacia-ob-ops-sm', [
            'content' => $content,
        ]);
    }
    public function actionInformaciaObIc()
    {
        $content = Page::find()->where(['slug'=>'informacia-ob-ic', 'status'=>Page::STATUS_PUBLISHED])->one();
        return $this->render('informacia-ob-ic', [
            'content' => $content,
        ]);
    }
    public function actionIndexorig()
    {
        return $this->render('index_orig');
    }
    public function actionAsk()
    {
        $model = new Questions();
        if ($model->load(Yii::$app->request->post())) {
             if($model->save()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        if(Yii::$app->request->isAjax) {
            return $this->renderAjax('_askform', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['site/index']);
        }
    }
        /**
     * Returns IDs of category and all its sub-categories
     *
     * @param Category[] $categories all categories
     * @param int $categoryId id of category to start search with
     * @param array $categoryIds
     * @return array $categoryIds
     */
    private function getCategoryIds($categories, $categoryId, &$categoryIds = [])
    {
        foreach ($categories as $category) {
            if ($category->id == $categoryId) {
                $categoryIds[] = $category->id;
            }
            elseif ($category->parent_id == $categoryId){
                $this->getCategoryIds($categories, $category->id, $categoryIds);
            }
        }
        return $categoryIds;
    }
    protected function findCategoryModel($id)
    {
        if(($model = Category::findOne($id)) != null)
        {
            return $model;
        } else {
            throw new NotFoundHttpException('The requsted page does not exist');
        }
    }

    protected function findProductModel($id)
    {
        if(($model = Product::findOne($id)) != null)
        {
            return $model;
        } else {
            throw new NotFoundHttpException('The requsted page does not exist');
        }
    }
}
