<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ArticleCategory;
use common\models\ArticleAttachment;
use yii\data\ActiveDataProvider;
use frontend\models\search\ArticleSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class ArticleController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $catid = 1;
        if(LANG=='en') {
            $catid = 3;
        } elseif(LANG=='kz') {
            $catid = 2;
        } else {
            $catid = 1;
        }
        $query = Article::find()->where(['status'=>1, 'category_id'=>$catid])->orderBy('published_at');
        $pagination = new Pagination(['totalCount' => $query->count(), 'pageSize' => 2]);

        $articles = $query->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
        
        return $this->render('index', [
            'category' => ArticleCategory::findOne($catid),
            'articles' => $articles,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $model = Article::find()->published()->andWhere(['slug'=>$slug])->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }

        $viewFile = $model->view ?: 'view';
        return $this->render($viewFile, ['model'=>$model]);
    }

    /**
     * @param $id
     * @return $this
     * @throws NotFoundHttpException
     * @throws \yii\web\HttpException
     */
    public function actionAttachmentDownload($id)
    {
        $model = ArticleAttachment::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }

        return Yii::$app->response->sendStreamAsFile(
            Yii::$app->fileStorage->getFilesystem()->readStream($model->path),
            $model->name
        );
    }
}
