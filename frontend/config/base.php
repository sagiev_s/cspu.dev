<?php
return [
    'id' => 'frontend',
    'name'=>'Центр сертификации продукции, услуг',
    'basePath' => dirname(__DIR__),
    'components' => [
        'urlManager' => require(__DIR__.'/_urlManager.php'),
        'cache' => require(__DIR__.'/_cache.php'),
    ],
];
