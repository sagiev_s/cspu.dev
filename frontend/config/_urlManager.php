<?php
return [
    'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    'rules'=> [
        // Pages
        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

        // Articles
        ['pattern'=>'news', 'route'=>'article/index'],
        ['pattern'=>'news/attachment-download', 'route'=>'article/attachment-download'],
        ['pattern'=>'news/<slug>', 'route'=>'article/view'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],

        // Site URL manager
        ['pattern'=>'/', 'route'=>'site/index'],// site
        ['pattern'=>'<parent>/<slug>', 'route'=>'site/page'],// site
        ['pattern'=>'<action>', 'route'=>'site/<action>'],
    ]
];
