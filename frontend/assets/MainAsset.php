<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'js/fancy/jquery.fancybox.css',
        'js/mscroll/jquery.mCustomScrollbar.css',
        'css/cspustyle.css',
        'css/media.css',
        'font-awesome/css/font-awesome.min.css',
    ];

    public $js = [
        'js/fancy/jquery.mousewheel-3.0.6.pack.js',
        'js/fancy/jquery.fancybox.js',
        'js/mscroll/jquery.mCustomScrollbar.concat.min.js',
        'js/mscroll/scrollLauncher.js',
        'js/new.js',
        'js/scroll.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\Html5shiv',
    ];
}
