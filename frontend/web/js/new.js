$('#category-tabs li a').click(function(){
    $(this).next('ul').slideToggle('500');
    $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
});

var showingTooltip;

    document.onmouseover = function(e) {
      var target = e.target;

      var tooltip = target.getAttribute('data-tooltip');
      if (!tooltip) return;

      var tooltipElem = document.createElement('div');
      tooltipElem.className = 'tooltip';
      tooltipElem.innerHTML = tooltip;
      document.body.appendChild(tooltipElem);

      var coords = target.getBoundingClientRect();

      var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
      if (left < 0) left = 0; // не вылезать за левую границу окна

      var top = coords.top - tooltipElem.offsetHeight - 5;
      if (top < 0) { // не вылезать за верхнюю границу окна
        top = coords.top + target.offsetHeight + 5;
      }

      tooltipElem.style.left = left + 'px';
      tooltipElem.style.top = top + 'px';

      showingTooltip = tooltipElem;
    };

    document.onmouseout = function(e) {

      if (showingTooltip) {
        document.body.removeChild(showingTooltip);
        showingTooltip = null;
      }

    };

jQuery(document).ready(function($) {
  // $('h3.togglerTitle').click(function(event) {
  //   $('h3.togglerTitle').removeClass('active');
  //   $(this).addClass('active');
  //   $('.togglerBlock').slideUp();
  //   $('.togglerBlock_' + $(this).attr('data-block')).slideDown();
  // });

   $('h3.CatTogglerTitle').click(function(event) {
     $('h3.CatTogglerTitle').removeClass('active');
     $(this).addClass('active');
     $('.CatTogglerBlock').hide();
     $('.CatTogglerBlock_' + $(this).attr('data-block')).slideDown('fast');
     var id = $(this).attr('data-block');
     $.get('/category-info', { id : id }, function(data) {
       var data = $.parseJSON(data);
       $('#catInfo').html('<p>Категория</p>'+'<h1>«'+data.name+'»</h1>');
     });
   });

  $('p.CatTogglerElem').click(function(event) {
    $('p.CatTogglerElem').removeClass('active');
    $(this).addClass('active');
    $('hr.productLine').show();
    $('div.productHelp').show(); 
    var id = $(this).attr('id');
    $.get('/code-info', { id : id }, function(data) {
      var data = $.parseJSON(data);
      $('#prodName').html('<h1>'+data.name+'</h1>'+'<span class="typecWarn"><p><img src="img/warning-icon.png"> '+data.content+'</p></span>'
        +'<p class="tnvedDesc">ТН ВЭД код — <span class="tnvedCode">'+data.code+'</span></p>'+'<input type="submit" class="prodOrderBtn" value="Заказать услугу">');
    });
  });
  $('section').click(function(event) {
    $(this).find('img').toggle();
  });

  $('a.askBtn').click(function(event) {
      var jqxhrAsk = $.ajax({
        url: $(this).attr('href'),
        type: 'GET',
        dataType: 'html',
        beforeSend: function( xhr ) {
          $('body').addClass('noscroll');
          $('.blackFade').fadeIn('fast');
          $('.blackFade').addClass('loading');
        },
      })
      .done(function(data) {
        console.log("success");
        $('.blackFade').removeClass('loading');
        $('.popupInnerWrap').html(data)
      })
      .fail(function() {
        console.log("error");
        $('body').removeClass('noscroll');
        $('.blackFade').fadeOut('fast');
        $('.blackFade').removeClass('loading');
      });
      
      return false;
  });

  $('.blackFade .closeBtn').click(function(event) {
    $('body').removeClass('noscroll');
    $('.popupInnerWrap').html('')
    $('.blackFade').fadeOut('fast');
    $('.blackFade').removeClass('loading');
    return false;
  });

  $(".fancybox").fancybox({padding : 0});
});
