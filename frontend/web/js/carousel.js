/**
 *  carousel launching scripts
 */
$(document).ready(function () {
	$(".owl-carousel").owlCarousel({
	  	loop: true,
	  	lazyLoad:true,
	    margin: 0,
	    nav: true,
	    dots: false,
	    responsiveClass: true,
	    items: 1,
	    navSpeed: 2000,
	    autoplaySpeed: 2000,
	    dotsSpeed: 2000,
	    autoplay: true,
	    autoplayTimeout: 6000,
	    autoplayHoverPause: true
	});
});

