<?php
/* @var $this yii\web\View */
/* @var $model common\models\Article */
use yii\helpers\Html;

$this->title = $model->title;
?>

<div class="content staticPage news">
    <article class="article-item">
        <h1 class="page-title"><?php echo $model->title ?></h1>
        <div class="created"><?=  Yii::$app->dateformatter->convert($model->published_at) ?></div>
        <div class="mainContent">
            <?php if ($model->thumbnail_path): ?>
                <?php echo \yii\helpers\Html::img(
                    Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $model->thumbnail_path,
                        'w' => 200
                    ], true),
                    ['class' => 'article-thumb img-rounded pull-left']
                ) ?>
            <?php endif; ?>

            <?php echo $model->body ?>
        </div>
        <?php if (!empty($model->articleAttachments)): ?>
            <h3><?php echo Yii::t('frontend', 'Attachments') ?></h3>
            <ul id="article-attachments">
                <?php foreach ($model->articleAttachments as $attachment): ?>
                    <li>
                        <?php echo \yii\helpers\Html::a(
                            $attachment->name,
                            ['attachment-download', 'id' => $attachment->id])
                        ?>
                        (<?php echo Yii::$app->formatter->asSize($attachment->size) ?>)
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

    </article>
    <?= Html::a(Yii::t('frontend', 'Show all news'), ['article/index'], ['class'=>'bigRedLink ease']) ?>
</div>