<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
$pageTitle = $category->title.' - '.Yii::$app->name;
$pageDesc = $content->seo_desc;
$pageUrl = Url::to(['article/index'], true);
$pageImage = '';
$ImageWidth = 480;
$ImageHeight = 360;

//setting title
$this->title = $pageTitle;
//setting meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => $ImageWidth]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => $ImageHeight]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $pageUrl]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'twitter:type', 'content' => 'summary']);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $pageDesc]);
?>
<div class="site-about staticPage">
	<h1 class="page-title"><?= $category->title ?></h1>
	<?php foreach ($articles as $key => $article): ?>
		<?php echo $this->render('_item', ['model'=>$article]) ?>
	<?php endforeach ?>

	<div class="pagerBlock">
        <?php
        echo LinkPager::widget([
            'pagination' => $pagination,
        ]);
        ?>
    </div>
</div>