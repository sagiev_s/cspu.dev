<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>
<hr/>
<div class="article-item row">
    <h3 class="article-title">
        <?php echo Html::a($model->title, ['view', 'slug'=>$model->slug]) ?>
    </h3>
    <div class="article-meta">
        <span class="created">
            <?php echo Yii::$app->dateformatter->convert($model->created_at) ?>
        </span>
    </div>
    <div class="article-content">
        <?php if ($model->thumbnail_path): ?>
            <?php echo Html::img(
                Yii::$app->glide->createSignedUrl([
                    'glide/index',
                    'path' => $model->thumbnail_path,
                    'w' => 100
                ], true),
                ['class' => 'article-thumb img-rounded pull-left']
            ) ?>
        <?php endif; ?>
        <div class="article-text">
            <?php echo \yii\helpers\StringHelper::truncate($model->body, 150, '...', null, true) ?>
        </div>
    </div>
</div>
