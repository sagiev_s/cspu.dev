<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainAsset;
use common\widgets\Alert;
use common\widgets\DbText;
use common\widgets\DbMenu;
use yii\widgets\Menu;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->request->baseUrl ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?= Yii::$app->request->baseUrl ?>/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?= Yii::$app->request->baseUrl ?>/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?= Yii::$app->request->baseUrl ?>/favicon/manifest.json">
	<link rel="mask-icon" href="<?= Yii::$app->request->baseUrl ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="cs_body <?= Yii::$app->controller->id.'_'.Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>
	<section class="mhead">
		<div class="main-head wrap1440">
			<div class="mnu-head">
				<ul class="top-mnu">
					<li>
						<a href="<?= Url::to(['site/ask']) ?>" class="askBtn">
							<img class="menuicon1" src="/img/rectangle-5.svg">
							<span><?= Yii::t('frontend', 'ask') ?></span>
						</a>
					</li>
					<li>
						<a href="<?= Url::to(['site/login']) ?>">
							<img class="menuicon2" src="/img/combined-shape.svg">
							<span><?= Yii::t('frontend', 'register_and_cabinet') ?></span>
						</a>
					</li>
					<li class="arrowLi">
						<?php
						// language selector
			        	$availableLocales = Yii::$app->params['availableLocales'];
						?>
						<a href="javascript:void(0);">
							<?= '<img class="menuicon" src="/img/'.Yii::$app->language.'.png" alt="'.$availableLocales[Yii::$app->language].'">'.$availableLocales[Yii::$app->language] ?>
						</a>
						<ul>
							<li class="active">
								<a href="javascript:void(0);">
									<?= '<img class="menuicon" src="/img/'.Yii::$app->language.'.png" alt="'.$availableLocales[Yii::$app->language].'">'.$availableLocales[Yii::$app->language] ?>
								</a>
							</li>
						<?php
						foreach ($availableLocales as $code => $lang) {
							if(Yii::$app->language != $code) {
								echo '<li>';
								echo Html::a('<img class="menuicon" src="/img/'.$code.'.png" alt="'.$availableLocales[$code].'">'.$availableLocales[$code], ['/site/set-locale', 'locale'=>$code]);
								echo '</li>';
							}
			        	}
						?>
						</ul>
					</li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="logoHolder">
			<div class="wrap1440">
				<div class="innerWrapper">
					<a href="<?= Url::home() ?>"><img src="/img/cspulogo_upd.png" class="mainlogo"></a>
					<hr class="blueline">
				</div>
			</div>
			<hr class="redline">
		</div>
	</section>
	<div class="contentWrapper wrap1440">
		<div class="sidebar">
			<input type="checkbox" id="menu-checkbox" />
			<div role="navigation">
			<label for="menu-checkbox" class="toggle-button" data-open="Меню" onclick></label>
			<?php echo DbMenu::widget([ 'key' => 'mainmenu', 'options' => ['class' =>'sidebar-mnu nav'] ]) ?>
			</div>
			<div class="call">
				<p><?= Yii::t('frontend', 'You have questions? Call us now!') ?></p>
				<h1><?= DbText::widget(['key' => 'phone']) ?></h1>
			</div>
			<a href="<?= Url::to(['site/ask']) ?>" class="askBtn redAskBnt ease"><?= Yii::t('frontend', 'ask') ?></a>
		</div>
		<div class="contentBar">
			<?= $content ?>
		</div>
		<div class="clear"></div>
	</div>
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2"></div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-sm-push-6 col-xs-12">
							<div class="useterm">
								<a href=""><?= Yii::t('frontend', 'Terms of use') ?></a>
								<a href=""><?= Yii::t('frontend', 'Policy') ?></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-sm-pull-6 col-xs-12">
							<div class="copyright">
								<p>&copy; Центр сертификации продукции, услуг <?= date('Y') ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="blackFade" style="display:none;">
		<?= Html::img('@web/js/fancy/fancybox_loading.gif', ['class'=>'loading']) ?>
		<div class="popup">
			<a href="#" class="closeBtn"><?= Html::img('@web/img/closeBtn.png', ['alt'=>Yii::t('frontend', 'Close'), 'title'=>Yii::t('frontend', 'Close')]) ?></a>
			<div class="popupInnerWrap"></div>
		</div>
	</div>
<?php $this->endBody() ?>
<a href="#0" class="cd-top"><?= Yii::t('frontend', 'Top') ?></a>
</body>
</html>
<?php $this->endPage() ?>