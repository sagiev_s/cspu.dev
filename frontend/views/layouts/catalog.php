<?php

use common\widgets\CategoriesWidget;
use yii\widgets\ActiveForm;
use common\models\Product;
use yii\jui\AutoComplete;
use common\models\Search;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Menu;
/* @var $this \yii\web\View */
/* @var $content string */

?>

<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-4">
	<?php
    //Global Search
    $form = ActiveForm::begin(
        [
        'action' => ['/site/search'],
        'method' => 'post',
        'options' => 
        ['class' => 'navbar-form navbar-left']
        ]);
    $listdata = Product::find()
        ->select(['code as value'])
        ->asArray()
        ->all();
    $model = new Search();
    ?>

    <div class="input-group">

    <?= $form->field($model, 'search')->widget(AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control',
            'placeholder' => 'Введите первые 4 цифры ТН ВЭД кода',
        ],
        'clientOptions' => [
            'source' => $listdata,
            'minLength'=>'4',
        ],
    ])->label(false);
    
    ?>

    <span class="input-group-btn">

    <?php
    echo Html::submitButton(
        '<span class="glyphicon glyphicon-search"></span>',
        [
            'class' => 'btn btn-success'
        ]
    );
    ?>
    </span></div>
    
    <?php
    ActiveForm::end();
    ?>
		<?= CategoriesWidget::widget([
                'category' => isset($this->params['category']) ? $this->params['category'] : null,
            ]) ?>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-8">
		<?=$content ?>
	</div>
</div>