<?php

	use yii\helpers\Html;
	use yii\helpers\Url;
	use common\widgets\DbText;
	use common\widgets\SearchFormWidget;

/* @var $this yii\web\View */
$pageTitle = Yii::$app->name;
$pageDesc = Yii::$app->name; //'Seo desc here'
$pageUrl = Url::to(['site/index'], true);
$pageImage = '';
$ImageWidth = 480;
$ImageHeight = 360;

//setting title
$this->title = $pageTitle;
//setting meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => $ImageWidth]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => $ImageHeight]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $pageUrl]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'twitter:type', 'content' => 'summary']);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $pageDesc]);
?>


<div class="slider-wrap">
	<?php echo \common\widgets\DbOwlCarousel::widget([
        'key'=>'index',
    ]) ?>
</div>
<div class="search-wrap">
	<h1><?= Yii::t('frontend', 'Search by code') ?></h1>
	<?= SearchFormWidget::widget(['inputclass' => 'searchbar', 'btnclass' => 'searchbtn', 'inputWrap' => 'searchMainInputs']) ?>

    <?= DbText::widget(['key' => 'hints']) ?>
</div>
<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-lg-10 col-lg-offset-1 col-xs-12">
				<div class="content-icon">
					<div class="row">
						<?= DbText::widget(['key' => 'services-mainpage']) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="app-wrap">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="app-online">
			<?= DbText::widget(['key' => 'online-request']) ?>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="app-download">
			<h1><?= Yii::t('frontend', 'Download application forms') ?></h1>
			<div class="download-links">
				<div class="scrollbar">
				<?php foreach ($documents as $key => $document): ?>
					<a href="<?= $document->file_base_url ?>/<?= $document->file_path ?>" class="ease" target="_blank">
						<?= Html::img('@web/img/'.$document->format.'.png', ['alt'=>$document->format]) ?>
						<?= $document->title ?>
					</a>
				<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php if ($news!==null && count($news)>0): ?>
	<div class="newsBlock">
		<h1><?= $newsCategory->title ?></h1>
		<?php foreach ($news as $key => $item): ?>
			<div class="newsItem">
				<span><?=  Yii::$app->dateformatter->convert($item->published_at) ?></span>
				<?= Html::a($item->title, ['article/view', 'slug'=>$item->slug]) ?>
			</div>
		<?php endforeach ?>
		<?= Html::a(Yii::t('frontend', 'Show all'), ['article/index'], ['class'=>'allNewsBtn ease']) ?>
	</div>
<?php endif ?>