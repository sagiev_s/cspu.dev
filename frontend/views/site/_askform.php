<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */
/* @var $form ActiveForm */
?>
<div class="askForm">

	<div class="questionFormHolder">
		<h1>Задать вопрос</h1>
		<?php $form = ActiveForm::begin([
			'id' => 'question-form',
		]); ?>

		<?php echo $form->field($model, 'name') ?>
		<?php echo $form->field($model, 'phone')->widget(MaskedInput::className(), [
			'mask' => '8 999 999 99 99',
		]) ?>
		<?php echo $form->field($model, 'email') ?>
		<?php echo $form->field($model, 'question')->textArea(['rows' => 5]) ?>
		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>

</div><!-- questions -->
