<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Menu;
use common\widgets\DbMenu;
/* @var $this \yii\web\View */
/* @var $content string */

?>

<?php echo DbMenu::widget([ 'key' => 'mainmenu', 'options' => ['class' =>'sidebar-mnu nav'] ]) ?>
<div class="history-item">
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-12">
            <div class="year">1997</div>
        </div>
        <div class="col-md-11 col-sm-11 col-xs-12 bordered">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste magni veritatis repellendus earum assumenda error hic ratione beatae dolorem alias! Recusandae error, deserunt nulla earum aperiam! Ut quos fugiat, eaque?</p>
        </div>
    </div>
</div>
<div class="history-item">
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-12">
            <div class="year">1997</div>
        </div>
        <div class="col-md-11 col-sm-11 col-xs-12 bordered">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste magni veritatis repellendus earum assumenda error hic ratione beatae dolorem alias! Recusandae error, deserunt nulla earum aperiam! Ut quos fugiat, eaque?</p>
        </div>
    </div>
</div>
