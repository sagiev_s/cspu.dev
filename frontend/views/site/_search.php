<?php
/*
 *     @var $model frontend\controller\SearchController
 *     @var $search frontend\controller\SearchController
 *     File: _search.php
 *     Purpose: Used for ListView for displaying search results
 *
 */
	use yii\helpers\Html;

?>
	<?php if(strlen(Html::encode($model->code))>6): ?>
		<h1><?= $model->name ?></h1>
		<p>Категория: <span style="font-weight: bold">«<?= Html::encode($model->category->name) ?>»</span></p>
		<span class="typecWarn"><img src="img/warning-icon.png"> <?= Html::encode($model->content) ?></span>
		<p>ТН ВЭД код — <span class="tnvedCode"><?= Html::encode($model->code) ?></span></p>
	<?php else: ?>
		<p>Категория: <span style="font-weight: bold">«<?= Html::encode($model->name) ?>»</span></p>
		<p>ТН ВЭД код — <span class="tnvedCode"><?= Html::encode($model->code) ?></span></p>
	<?php endif; ?>
					