<?php

	use yii\helpers\Html;
	use common\widgets\SearchFormWidget;

?>

<div class="treeContent">
	<h1 class="topTreeH">Введите первые 4 цифры ТН ВЭД кода</h1>
	<?= SearchFormWidget::widget() ?>
	<div class="treeCategory" id="catInfo"></div>
	<div class="treeListWrapper">
		<div class="container">
			<div class="treeList">
				<div class="treeListCont">
					<ul class="catsList">
						<?php foreach ($categories as $category): ?>
						<?php if ($products = $category->products): ?>
						<li>
								<h3 class="CatTogglerTitle" data-block="<?= Html::encode($category->id) ?>"><?= Html::encode($category->name) ?></h3>
								<ul class="CatTogglerBlock CatTogglerBlock_<?= Html::encode($category->id) ?>" style="display: none;">
								<?php foreach ($products as $prod): ?>
								<li><p class="CatTogglerElem" id="<?= Html::encode($prod->id) ?>"><?= Html::encode($prod->name) ?></p></li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
					</ul>

				</div>
			</div>
			<div class="treeInfo">
				<div class="productInfo" id ="prodName"></div>
				<hr class="productLine"  style="display: none">
				<div class="productHelp" style="display: none">
					<p>Если Вы не смогли найти нужный вам раздел <br> инаименование товара или услуги,</p>
					<input type="submit" class="applyOnlBtn" value="подайте заявку онлайн">
					<p>и мы Вам поможем!</p>
				</div>
			</div>
		</div>
	</div>
</div>
