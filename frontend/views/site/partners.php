<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\DbMenu;

/* @var $this yii\web\View */
$pageTitle = ($content->seo_title!==null && !empty($content->seo_title)) ? $content->seo_title : $content->title;
$pageDesc = $content->seo_desc;
$pageUrl = Url::to(['site/about'], true);
$pageImage = '';
$ImageWidth = 480;
$ImageHeight = 360;

//setting title
$this->title = $pageTitle;
//setting meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => $ImageWidth]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => $ImageHeight]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $pageUrl]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'twitter:type', 'content' => 'summary']);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $pageDesc]);


// $this->registerJsFile('@web/js/fancy/jquery.mousewheel-3.0.6.pack.js', [ 'position' => \yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()] ]);
// $this->registerJsFile('@web/js/fancy/jquery.fancybox.js', [ 'position' => \yii\web\View::POS_END, 'depends' => [\yii\web\JqueryAsset::className()] ]);
// $this->registerCssFile('@web/js/fancy/jquery.fancybox.css');
// $this->registerJs('$(".fancybox").fancybox({padding : 0});', yii\web\View::POS_READY);
?>

<div class="page-view staticPage">
	<h1 class="page-title"><?= $content->title ?></h1>

	<div class="belowContent">
		<?= DbText::widget(['key' => 'partners-temp1']) ?>
		<div class="clear"></div>
	</div>

	<div class="mainContent customScroll">
		<?= $content->body ?>
	</div>

		<div class="belowContent">
		<?= DbText::widget(['key' => 'partners-temp2']) ?>
		<div class="clear"></div>
	</div>

	<div class="underContent submenu">
		<?= DbMenu::widget(['key' => 'about-menu', 'options' => ['class' =>'bluemenu'],  'linkTemplate' => '<a href="{url}" class="blueArrowedBtn ease"><span>{label}</span></a>']) ?>
		<div class="clear"></div>
	</div>
</div>