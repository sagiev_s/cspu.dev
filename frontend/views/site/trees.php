<?php

use yii\helpers\Html;
use yii\widgets\ListView;


$this->title = 'Категории';
?>

<div class="catalog-index">
	<h1><?=Html::encode($this->title) ?></h1>
	<?=ListView::widget([
			'dataProvider' => $dataProvider,
			'layout' => "{items}\n{pager}",
			'itemView' => '_item',
		]);
	 ?>
</div>