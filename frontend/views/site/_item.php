<?php

use yii\helpers\Html;

?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h1 style="font-size: 16px"><?= $model->name ?></h1>
	</div>
	<div class="panel-body">
		<p>Category: <?=$model->category ? Html::a(Html::encode($model->category->code), ['category', 'id' => $model->category->id]) : 'Без категории' ?></p>
		<div><p style="color:red"><?=$model->content ?></p></div>
		<div><p>ТН ВЭД код -<?=$model->code ?></p></div>
	</div>
</div>