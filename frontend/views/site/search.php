<?php

	use yii\helpers\Html;
	use yii\widgets\ListView;
	use common\widgets\SearchFormWidget;

?>
<div class="treeContent">
<?= SearchFormWidget::widget() ?>	
	<div class="treeListWrapper">
		<div class="container">	
			<div class="treeList">	
				<div class="productInfo">
					<?= ListView::Widget([
								'dataProvider' => $dataProvider,
								'itemView' => '_search',
								'viewParams' => 
								[
									'search' => $search
								],
								'layout' => "\n{items}",
						]);
					?>
					<input type="submit" class="prodOrderBtn" value="Заказать услугу">
				</div>
			</div>
			<div class="treeInfo">
				<div class="productHelp">
					<p>Если Вы не смогли найти нужный вам раздел <br> инаименование товара или услуги,</p>
					<input type="submit" class="applyOnlBtn" value="подайте заявку онлайн">
					<p>и мы Вам поможем!</p>
				</div>
			</div>
		</div>		
	</div>
</div>