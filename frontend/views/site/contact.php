<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\DbText;

/* @var $this yii\web\View */
$pageTitle = ($content->seo_title!==null && !empty($content->seo_title)) ? $content->seo_title : $content->title;
$pageDesc = $content->seo_desc;
$pageUrl = Url::to(['site/contact'], true);
$pageImage = '';
$ImageWidth = 480;
$ImageHeight = 360;

//setting title
$this->title = $pageTitle;
//setting meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => $ImageWidth]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => $ImageHeight]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $pageUrl]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'twitter:type', 'content' => 'summary']);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $pageDesc]);
?>

<div class="site-contact staticPage">
  <div class="mainContent">
    <div id="map"></div>
    <?= $content->body ?>
  </div>
</div>

<div class="clear"></div>

<script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: new google.maps.LatLng(43.257908, 76.961430),
          mapTypeId: 'roadmap'
        });

        var marker = new google.maps.Marker({
	        position: map.getCenter(),
	        icon: "<?= Url::to('@web/img/marker.png?v=2') ?>",
	        map: map
	      });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijuzTPTDrv8gYNjb0UqJJPfmv24HAZhU&callback=initMap">
    </script>