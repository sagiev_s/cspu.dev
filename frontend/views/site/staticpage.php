<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Menu;

/* @var $this yii\web\View */
$pageTitle = ($content->seo_title!==null && !empty($content->seo_title)) ? $content->seo_title : $content->title;
$pageTitle .= ' - '.Yii::$app->name;
$pageDesc = $content->seo_desc;
$pageUrl = Url::to(['page/view', 'slug'=>$_GET['slug']], true);
$pageImage = '';
$ImageWidth = 480;
$ImageHeight = 360;

//setting title
$this->title = $pageTitle;
//setting meta tags
$this->registerMetaTag(['name' => 'description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => $ImageWidth]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => $ImageHeight]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $pageUrl]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $pageDesc]);

$this->registerMetaTag(['property' => 'twitter:type', 'content' => 'summary']);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $pageTitle]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $pageImage]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $pageDesc]);
?>

<div class="page-view staticPage">
	<h1 class="page-title"><?= $content->title ?></h1>

	<div class="mainContent customScroll">
		<?= $content->body ?>
	</div>

	<div class="underContent submenu">
		<?= DbMenu::widget(['key' => 'about-menu', 'options' => ['class' =>'bluemenu'],  'linkTemplate' => '<a href="{url}" class="blueArrowedBtn ease"><span>{label}</span></a>']) ?>
		<div class="clear"></div>
	</div>
</div>